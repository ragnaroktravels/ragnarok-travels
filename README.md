# Ragnarok Travels Development

![Ragnarok Travels](resources/assassin_m.png)![Ragnarok Travels](resources/crusader_f.png)![Ragnarok Travels](resources/monk_m.png)![Ragnarok Travels](resources/rogue_f_2.png)

This is an issue and suggestion tracker for [Ragnarok Travels](https://www.ragnaroktravels.com), an authentic RO private server.

# Suggestion Tracker

Suggestions on **features** or **improvements** can be made here. **Note that we strictly adhere to official RO development, and we will not budge on this**. In fact, due to our uniquely accurate backend, most deviations are impossible. This limits the scope of things we will consider. Nevertheless we are open to quality of life suggestions as long as they don't affect game balance. 

Examples of suggestions that do not affect game balance:

* Opt-in chat channels (Status: implemented but disabled)
* Opt-in showexp (Status: implemented)

Examples of suggestions that do:

* autoloot (Status: rejected)
* Almost every other feature you see on low rate servers

Things such as the extent of god item availability and what high end cards are disabled in PvP environments varied quite drasitically between official servers, and can be discussed here.

# Issue Tracker

**Bugs** and **issues** can also be reported here. **Please note that, with the exception of very few cases, we will not change official behavior**. We are particularly interested in exploits and typographical (spelling, grammar, formatting) issues.

## Game
In-game issues should be reported here.

## Services (Avarice, Database, Wiki, Website)
Issues with Ragnarok Travels services should be reported here. Attach a screenshot and enough context to reproduce the issue.

# Links

* [Main site](https://www.ragnaroktravels.com)
* [Control panel](https://account.ragnaroktravels.com)
* [Wiki](https://wiki.ragnaroktravels.com/)
* [Discord](https://discord.gg/bKnC3dt)

![Ragnarok Travels](resources/1002.gif)
